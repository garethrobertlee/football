const regions =


[{
	name: "Calderdale",
	topoJson: "E08000033",
	locations: [
	 {name:"Halifax",coords: [-1.8575,53.7270],dataset: [3,22, 5,1]},
   {name:"Brighouse", coords:[-1.7825,53.6997]}
   ],
   scale:73213.92708456448,
   center:[-1.9502583064355525,53.720609076279096],
	key:1,
},
{
	name: "Kirklees",
	topoJson: "E08000034",
	locations: [
  {name:"Huddersfield", "WD13NM": "Huddersfield",coords:[-1.7850,53.6458]},
   ],
   center:[-1.7850,53.6458],
   scale:63213.92708456448,
	key:2,
},
{
	name: "Bradford",
	topoJson: "E08000032",
	locations: [
  {name:"Bradford", "WD13NM": "Bradford",coords:[-1.7600,53.8036]},
   ],
   center:[-1.8600,53.8536],
   scale:58213.92708456448,
	key:3,
},
{
	name: "Leeds",
	topoJson: "E08000035",
	locations: [
  {name:"Leeds", "WD13NM": "Leeds",coords:[-1.5491,53.8008]},
  {name:"Harlech", "WD13NM": "Harlech",coords:[-4.1079,52.8589]}
   ],
   center:[-1.5491,53.8008],
   scale:58213.92708456448,
	key:4,
}
]

export default regions;