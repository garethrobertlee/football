function region(state = null, action){
 
	if (action.type == 'CHANGE_REGION'){
		return action.event
	}
	return state
}

export default region;