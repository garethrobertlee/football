import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import regions from './regions.js';
import region from './region.js';
import ward from './ward.js';
import football from './football.js';
import chartFootball from './showFootball.js';

const rootReducer = combineReducers({
  regions,
  region,
  ward,
  football,
  chartFootball,
  routing: routerReducer,
});

export default rootReducer;