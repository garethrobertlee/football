import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './RegionChooser.css';
import moreStyles from './header/Header.css';
import changeRegion from '../actions/actionCreators';
import showWard from '../actions/showWard';
import showFootball from '../actions/showFootball';

console.log(moreStyles)

class RegionChooser extends Component {

  constructor(props) {
    super();
  }

  choosingARegion(region) {
    this.props.changeRegion(region);
    this.props.showWard(null)
    this.props.showFootball(null)
  }

    returnRegions(regions){
         {/*<ul className={styles.navItems}>
                {links.map((link) => <li  key={link} ref={link} className={styles.navItem}><Link to={`/${link}`}>{link}</Link></li>)}            
                </ul>*/}
        return regions.map((region,i) => (<span className={region.name == this.props.region.name ?  moreStyles.navItemActive : moreStyles.navItem} onClick={() => this.choosingARegion(region)} key={i}>{region.name}</span>))
    }

    render () {
         const regions = this.props.regions;
        return (
            <ul className={moreStyles.navItems}>{this.returnRegions(regions)}</ul>
        )
    }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
     changeRegion: changeRegion,
     showWard: showWard,
     showFootball: showFootball,
  }, dispatch);
}

RegionChooser.propTypes = {
  region: React.PropTypes.shape({
    center: React.PropTypes.arrayOf(React.PropTypes.number),
    locations: React.PropTypes.arrayOf(
        React.PropTypes.shape({
          WD13NM: React.PropTypes.string,
          name: React.PropTypes.string,
          coords: React.PropTypes.arrayOf(React.PropTypes.number),
          dataset: React.PropTypes.arrayOf(React.PropTypes.number),
        }),
      ),
    name: React.PropTypes.string,
    scale: React.PropTypes.number,
    topoJson: React.PropTypes.string,
  }),
  changeRegion:React.PropTypes.func
};

RegionChooser.defaultProps = {
  region: {},
};

export default connect(null, mapDispatchToProps)(RegionChooser);
