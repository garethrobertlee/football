import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actionCreators from '../actions/actionCreators.js';



import Main from './Main.js';
import css from './App.css';



function mapStateToProps(state){
	console.log(state)
	return {
		regions:state.regions,
		region:state.region || state.regions[1],
	
	}
}

const App = connect(mapStateToProps, null)(Main);

export default App;