import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import { bindActionCreators } from 'redux';
import styles from './Mapping.css';

import RegionChooser from './RegionChooser';
import Donut from './Donut';
import { connect } from 'react-redux';
import showWard from '../actions/showWard';
import setFootball from '../actions/setFootball';
import showFootball from '../actions/showFootball';

const classNames = require('classnames');

class Mapping extends Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.handleClick = this.handleClick.bind(this);
    this.showDonut = this.showDonut.bind(this);
  }

  componentDidMount() {
 
    this.makingAMap(this.props.region);
    this.getFootball(this.props.region);
  }

  componentWillReceiveProps(nextProps) {
   
    if (nextProps.region != this.props.region){
      this.getFootball(nextProps.region);
      this.makingAMap(nextProps.region);
    }
  }

  getFootball(region) {
    const self = this;
    const url = `data/${region.name}/teams.json`;
    d3.json((url), (error, geodata) => self.props.setFootball(geodata));
  }

  showWardFootball(region) {
    const ward = this.props.football.filter(s => s.INDICATOR.replace(/\s+/g, "") === region.properties.WD13NM.replace(/\s+/g, ""))[0];
    ward ? this.props.showFootball(ward) : this.props.showFootball(null)
  }

  makingAMap(region) {
  
    var el = ReactDOM.findDOMNode(this);
    
    d3.select('#datamapcontainer').selectAll('svg').remove();
    let width = Math.min(el.clientWidth,600);
    let height = 478

    let responsiveScale = Math.min(region.scale/(600/el.clientWidth),region.scale)

    const svg = d3.select('#datamapcontainer').append('svg')
    .attr('width', width)
    .attr('height', height);
   
    const projection = d3.geoMercator()
    .scale(responsiveScale)
    .center(region.center) // projection center
    .translate([width / 2, height / 2]); // translate to center the map in view

    const path = d3.geoPath()
    .projection(projection);

    const features = svg.append('g')
    .attr('class', 'features');

    const zoom = d3.zoom()
    .scaleExtent([1, Infinity]);

    svg.call(zoom);
    const self = this;
    const locs = region.locations;
    const url = `data/topo_${region.topoJson}.json`;

    d3.json((url), (error, geodata) => {
      this.setState({
        features:features,
        path:path,
        projection:projection,
        svg:svg,
        geodata:geodata

      })
      features.selectAll('path')
        .data(topojson.feature(geodata, geodata.objects[Object.keys(geodata.objects)[0]]).features)
        .enter()
        .append('path')
        .attr('d', path)
        .attr('class', d =>  d.properties.WD13NM.replace(/\s+/g, "").toLowerCase())
        .attr('id', d => d.properties.WD13NM.replace(/\s+/g, "").toLowerCase())
        .attr('fill', 'cornflowerblue')
        .on('click', (d, i) => self.handleClick(d, i));

      svg.selectAll('.percentage-label')
        .data(topojson.feature(geodata, geodata.objects[Object.keys(geodata.objects)[0]]).features)
        .enter().append('text')
        .attr('class', styles.placeLabel)
        .attr('transform', d =>  `translate(${projection(d3.geoCentroid(d))})`)
        .attr('dy', '.55em')
        .attr('id', d => `${d.properties.WD13NM.replace(/\s+/g, "").toLowerCase()}-percentage`)

      // add labels to dots
  

      svg.selectAll('thiscanbeanything')
        .data(locs).enter()
        .append('circle')
        .attr('cx', d => projection(d.coords)[0])
        .attr('cy', d => projection(d.coords)[1])
        .attr('r', '8px')
        .attr('fill', 'tomato');

      svg.selectAll('.place-labele')
        .data(locs)
        .enter().append('text')
        .attr('class', styles.placeLabel)
        .attr('transform', d => `translate(${projection(d.coords)})`)
        .attr('dy', '.55em')
        .text(d => d.name);

      return true;
    });
  }

  handleClick(d) {
    const highlight = `#${d.properties.WD13NM.replace(/\s+/g, "").toLowerCase()}`
    d3.selectAll('path').attr('fill','cornflowerblue')
    d3.select(highlight)
      .attr("fill", "tomato")

    this.props.showWard(d)
    this.showWardFootball(d);
  }

  showDonut() {
    if (this.props.chartFootball) {
      return (
        <Donut local={this.props.chartFootball} />
      );
    }
    return null
  }

  shadeRegion(team){

    team === 'All' ? this.setState({activeTeam:false}) : this.setState({activeTeam:team})
    const niceTeam = team.replace(/\s+/g, "").toLowerCase()
    const colours = ['red','blue','mistyrose','purple','grey']
    const shadings = this.props.football.map(shade => {
      const teamshade = `#${shade.INDICATOR.replace(/\s+/g, "").toLowerCase()}`
      const label = `${teamshade}-percentage`
      d3.select(teamshade)
      .attr("class", function(d) { return styles[niceTeam]})
      .attr("opacity", function(d) { console.log((shade[team]/100)) ; return (shade[team]/100 + 0.015)})
      d3.select(label)
      .attr("class", function(d) { return styles[`${niceTeam}text`]})
      .text(shade[team] > 0 ? shade[team]: "")
    })
  }

  showTeamSelectors(){
    if (this.props.football){
      
      const teams = [...Object.keys(this.props.football[0]).filter(key => (key != "INDICATOR")), this.state.activeTeam ? "All" : null]
   
      return (
        <div className={styles.teamChooser}>
          {teams.map(team => <span className={team === this.state.activeTeam ? styles.active : ''} key={team} onClick={() => this.shadeRegion(team)}>{team}</span>)}
        </div>)
      }
      return null
  }

  render() {
    const centre = classNames(styles.module, styles.moduleShort);

    return (
      <div className={styles.mapView}>   
        <div className={styles.left} id="datamapcontainer">
          {this.showTeamSelectors()}
        </div>
        <div className={styles.right}>
          <RegionChooser {...this.props} />
            {this.showDonut()}
        </div>
      </div>
    );
  }
}

Mapping.propTypes = {
  region: React.PropTypes.shape({
    center: React.PropTypes.arrayOf(React.PropTypes.number),
    locations: React.PropTypes.arrayOf(
        React.PropTypes.shape({
          WD13NM: React.PropTypes.string,
          name: React.PropTypes.string,
          coords: React.PropTypes.arrayOf(React.PropTypes.number),
          dataset: React.PropTypes.arrayOf(React.PropTypes.number),
        }),
      ),
    name: React.PropTypes.string,
    scale: React.PropTypes.number,
    topoJson: React.PropTypes.string,
  }),
};

Mapping.defaultProps = {
  region: {},
};

function mapStateToProps(state){
	console.log(state)
	return {
		football: state.football,
    chartFootball: state.chartFootball
	}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
     showWard: showWard,
     setFootball: setFootball,
     showFootball: showFootball
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Mapping);
