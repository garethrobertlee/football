import React, { Component } from 'react';
import { Link } from 'react-router';

import styles from './Change.css';
import Header from './Header.js';



class Main extends Component {

  constructor() {
    super();

   }

  render() {
    return (
      <div className={styles.container}>
        <Header {...this.props}/>
          {React.cloneElement(this.props.children, this.props)}
        </div>
    );
  }
}

export default Main;


