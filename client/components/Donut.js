import React, { Component } from 'react';
import * as d3 from 'd3';
import ReactDOM from 'react-dom'
import styles from './Donut.scss';
import { connect } from 'react-redux';


function translate(x, y) {
  return `translate(${x}, ${y})`;
}

class Slice extends Component {
  constructor(props) {
    super(props);
    this.state = {isHovered: false};
    this.renderLabel = this.renderLabel.bind(this)
  }



  renderLabel(label,value){
    if (value > 5){
      return value
    }
    return null
  }

  render() {
    let {value, activeSlice, label, fill, innerRadius = 0, outerRadius, cornerRadius, padAngle, ...props} = this.props;

    const shirt = label.replace(/\s+/g, "").toLowerCase()
    const shirttext = `${shirt}text`

    let arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .cornerRadius(cornerRadius)
      .padAngle(padAngle);
    return (
      <g onMouseOver={this.onMouseOver}
         onMouseOut={this.onMouseOut}
         {...props}>
        <path d={arc(value)} className={styles[shirt]} />
        <text transform={translate(...arc.centroid(value))}
              dy="0.35em"
              className={styles[shirttext]}
              >
          {this.renderLabel(label,value.data.value)}
        </text>
      </g>
    );
  }
}

class Pie extends Component {
  constructor(props) {
    super(props);
    this.colorScale = d3.scaleOrdinal(d3.schemeCategory10);
    this.renderSlice = this.renderSlice.bind(this);
  }

  render() {
   
    let {x, y, data} = this.props;
    console.log(data)
    let pie = d3.pie()  
    pie.value(function(d){
      if (d){
        console.log(d)
    return d.value; 
    }
   });
  
    return (
    
      <g transform={translate(x, y)}>
        {pie(data).map(this.renderSlice)}
        <text className={styles.chartLabelText} transform={translate(-210,-170)}>{this.props.label}</text>
      </g>
    );
  }

  renderSlice(value, i) {
    let {innerRadius, outerRadius, cornerRadius, padAngle, activeSlice} = this.props;

    
    return (
      <Slice key={i}
             innerRadius={innerRadius}
             outerRadius={outerRadius}
             cornerRadius={cornerRadius}
             padAngle={padAngle}
             value={value}
             label={value.data.label}
             activeSlice={activeSlice}
            />
    );
  }
}

class Donut extends Component {

  constructor(props) {
    super(props);
    this.state = {
      height: null
    };
  }


  componentDidMount(){

    var node = ReactDOM.findDOMNode(this);
    var u = ReactDOM.findDOMNode(this.refs.hi);
    this.setState({
      width:u.clientWidth,
      height:u.clientHeight
    })
  }

 renderChart(){

    let data = this.props.local || [];
    let width = window.innerWidth /2;
    let height = window.innerHeight /2;
    let minViewportSize = Math.min(this.state.width, this.state.height);
    let radius = (minViewportSize * .6) / 2;
    let x = width / 2;
    let y = height / 2;
    let region = this.props.region
    let chosenWard = this.props.local
    let thedata = Object.keys(data).map(function (key) { if (typeof(data[key]) === 'number') return {label:key, value:data[key]} }).filter(function(v){return v != undefined});
    let thelabel = chosenWard.INDICATOR
    if (this.state.width){

      return (
        <svg className={styles.svg} label={thelabel}>
       
          <Pie x={this.state.width/2}
             y={this.state.height/2}
             innerRadius={radius * .35}
             outerRadius={radius}
             cornerRadius={7}
             padAngle={.02}
      
             label={thelabel}
             data={thedata} />
        </svg>
      )
    }
    
    return <div className={styles.svg}></div>
 }

  render() {
    
    let chosenWard = this.props.local
        return (
          <div className={styles.donuts} ref="hi" id="mossley">
            {this.renderChart()}
          </div>
        );
     }
  }

// export default Donut;
function mapStateToProps(state) {
  console.log(state)
  return {
    ward: state.ward
  }
}

export default connect(mapStateToProps, null)(Donut);
