import React, { Component } from 'react';

import styles from './header/Header.css';
import { Link } from 'react-router';

const classNames = require('classnames');


class Header extends Component {

  constructor() {
    super();
   }

  render() {

    const links = ['building','helping','about','mapping']
    const active = this.props.routes[1].path
    const top =  classNames(styles.header,styles.container);
    const second = classNames(styles.row,styles.rowHeader)
    const third = classNames(styles.col,styles.colHeader)
    return (
      <header className={top}>
          <div className={second}>
            <div className={third}>
              <Link className={styles.headerLogo} to={`/`}>Yorkshire Football</Link>
            </div>
            <div className={third}>
              <nav className={styles.nav}>
              </nav>
            </div>
          </div>
        </header>
    );
  }
}

export default Header;

